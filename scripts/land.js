var camera, scene, renderer,
    geometry, material, mesh;

init();
animate();

function init() {

    clock = new THREE.Clock();

    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.z = 1000;
    scene.add(camera);

    geometry = new THREE.CubeGeometry(300, 300, 300);
    material = new THREE.MeshLambertMaterial({ color: 0xaa6666, wireframe: false });
    mesh = new THREE.Mesh(geometry, material);
    cubeSineDriver = 0;

    light = new THREE.DirectionalLight(0xffffff, 0.25);
    light.position.set(-1, 0, 1);
    scene.add(light);

    smokeTexture = THREE.ImageUtils.loadTexture('/images/bg.png');
    smokeGeo = new THREE.PlaneGeometry(512, 512);
    smokeParticles = [];

    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight || e.clientHeight || g.clientHeight;
    if (w.x < 600) {
        var loop = 10;
        var color = '0x4a4a4a';
    } else {
        var loop = 20;
        var color = '0x2a2a2a';
    }
    for (p = 0; p < loop; p++) {
        var smokeMaterial = new THREE.MeshLambertMaterial({ color: color, map: smokeTexture, transparent: true });
        var particle = new THREE.Mesh(smokeGeo, smokeMaterial);
        particle.position.set(Math.random() * 500 - 250, Math.random() * 500 - 250, Math.random() * 1000 - 150);
        particle.rotation.z = Math.random() * 360;
        scene.add(particle);
        smokeParticles.push(particle);
    }

    if (!hasClass(document.getElementById('container'), 'shown')) {
        addClass(document.getElementById('container'), 'shown');
        document.getElementById('container').appendChild(renderer.domElement)
    }

}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function addClass(el, className) {
    if (el.classList)
        el.classList.add(className)
    else if (!hasClass(el, className)) el.className += " " + className
}

function animate() {
    delta = clock.getDelta();
    requestAnimationFrame(animate);
    evolveSmoke();
    render();
}

function evolveSmoke() {
    var sp = smokeParticles.length;
    while (sp--) {
        smokeParticles[sp].rotation.z += (delta * 0.125);
    }
}

function render() {
    mesh.rotation.x += 0.005;
    mesh.rotation.y += 0.01;
    cubeSineDriver += .01;
    mesh.position.z = 100 + (Math.sin(cubeSineDriver) * 500);
    renderer.render(scene, camera);
}

window.addEventListener('resize', renderer.setSize(window.innerWidth, window.innerHeight));
