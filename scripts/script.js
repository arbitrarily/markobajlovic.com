// Some Custom Garbage
jQuery(function($) {

    var mb = {

        fastclick: function() {
            $(function() {
                FastClick.attach(document.body);
            });
        },

        is_mobile: function() {
            var is_mobile = false; //initiate as false
            // device detection
            if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) is_mobile = true;
            return is_mobile;
        },

        increment: function() {
            $('.the-sum').delay(1000).queue(function() {
                $('.the-sum').each(function() {
                    var num = $(this).data('sum');
                    var step = $.animateNumber.numberStepFactories.separator(',');
                    $(this)
                        .prop('number', 1)
                        .animateNumber({
                                number: num,
                                numberStep: step
                            },
                            4444
                        );
                }).dequeue();
            });
        },

        slab: function() {
            $(".name.logo h1").slabText({
                "viewportBreakpoint": 280,
                "maxFontSize": 36
            });
        },

        flowtype: function() {
            $("body").flowtype({
                minimum: 600,
                maximum: 1200,
                minFont: 18,
                maxFont: 24,
                fontRatio: 60
            });
        },

        roles: function() {
            var $header = $("span.roles");
            var header = [
                'a <strong>jack of all trades</strong>.',
                'a <strong>full stack developer</strong>.',
                '<strong>professional</strong>.',
                'a <strong>creative</strong>.',
                '<strong>humble</strong>.',
                'an <strong>engineer</strong>.',
                'a <strong>futurist</strong>.',
                'a <strong>team leader</strong>.',
                'an <strong>artist</strong>.',
                'a <strong>student</strong>.',
                '<strong>helpful</strong>.',
                'an <strong>entrepreneur</strong>.',
                'a <strong>teacher</strong>.',
                'an <strong>innovator</strong>.',
                'a <strong>casual skater</strong>.',
                '<strong>agile</strong>.',
                'a <strong>scientist</strong>.',
                '<strong>informed</strong>.',
                '<strong>inspired</strong>.',
                '<strong>cultured</strong>.'
            ];
            var position = -1;
            ! function loop() {
                position = (position + 1) % header.length;
                $header.html(header[position])
                    .fadeIn(800)
                    .delay(1500)
                    .fadeOut(800, loop);
            }();
        },

        fonts: function() {
            // Google Fonts
            WebFont.load({
                google: {
                    families: ['Noto+Sans']
                },
                timeout: 1000,
                classes: false,
                events: false,
                text: 'abcdefghijklmnopqrstuvwxyz!@$%&*()[]{}=-_+,.`/',
                active: function() {
                    // Store in Session
                    sessionStorage.fonts = true;
                }
            });
            // Check for Font
            var font = new FontFaceObserver('Noto Sans', {
                weight: 400
            });
            font.load().then(function() {
                mb.slab(); // responsive titles slab
                $('html').addClass('font-active');
            });
            // Fallback
            setTimeout(function() {
                if (!$('html').hasClass('font-active')) {
                    mb.slab(); // responsive titles slab
                    $('html').addClass('font-active');
                }
            }, 1000);
        },

        set_about_height: function() {
            $('.about-me').height($(window).height());
        },

        scrollbar: function() {
            if (!mb.is_mobile()) {
                var options = {
                    speed: 0.8,
                    damping: 0.08,
                    overscrollDamping: 0.33,
                    overscrollEffect: 'bounce',
                    renderByPixels: true
                };
                Scrollbar.initAll(options);
            }
        }

    }

    // Fire Them Up
    mb.fastclick(); // fastclick
    mb.flowtype(); // responsive type
    mb.fonts(); // fonts
    mb.set_about_height(); // set height

    $(window).on('resize', function() {
        mb.set_about_height(); // set height
    });

    $(window).on('load', function() {
        mb.increment(); // incrememnt experience
        mb.roles(); // incrememnt experience
        mb.scrollbar(); // fonts
    });

    $('.the-sum').on('click touch', function() {
        mb.increment(); // incrememnt experience
    });

    $(window).on('beforeunload', function() {
        $("body").fadeOut();
    });

});
