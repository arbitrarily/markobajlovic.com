
User-agent: *
Allow: /
#Disallow: /

Sitemap: http://www.markobajlovic.com/sitemap.xml

##### Directories #####

Disallow: /404/
Disallow: /grunt/
Disallow: /app/
Disallow: /cgi-bin/
Disallow: /downloader/
Disallow: /includes/
Disallow: /lib/
Disallow: /pkginfo/
Disallow: /report/
Disallow: /stats/
Disallow: /var/

##### Paths (clean URLs) #####

Disallow: /css/
Disallow: /fonts/
Disallow: /images/
Disallow: /releases/
Disallow: /scripts/

##### Files #####

Disallow: /cron.php
Disallow: /cron.sh
Disallow: /error_log
Disallow: /install.php
Disallow: /LICENSE.html
Disallow: /LICENSE.txt
Disallow: /LICENSE_AFL.txt
Disallow: /STATUS.txt

Disallow: /*?*

User-agent: Screaming Frog SEO Spider
Allow: /
Disallow: /*.gif$
Disallow: /*.jpg$
Disallow: /*.png$
Disallow: /*.bmp$
Disallow: /*.xml$
Disallow: /*.css$
Disallow: /*.js$