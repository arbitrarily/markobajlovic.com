///////////////////////////////////////
// Grunt Config
///////////////////////////////////////

module.exports = function(grunt) {
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        grunt: {
            files: ["Gruntfile.js"],
            tasks: ["default"]
        },
        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: ['pkg'],
                commit: false,
                push: false,
                createTag: false
            }
        },
        watch: {
            options: {
                dateFormat: function(time) {
                    grunt.log.writeln('Watch finished in ' + time + 'ms at' + (new Date()).toString());
                    grunt.log.writeln('Waiting for more changes...');
                    grunt.log.writeln(' ');
                    grunt.log.writeln(' ');
                },
            },
            css: {
                files: ['../css/*.css', '../sass/*.scss'],
                tasks: ['sass', 'cssmin'],
            },
            scripts: {
                files: '../scripts/*.js',
                tasks: ['uglify'],
                options: {
                    interrupt: true,
                },
            },
        },
        sass: {
            dist: {
                files: {
                    '../css/style.css': '../sass/style.scss'
                }
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: 0
            },
            css: {
                src: [
                    '../css/reset.css',
                    '../css/font-awesome.css',
                    '../css/foundation.css',
                    '../css/style.css'
                ],
                dest: '../style.min.css'
            }
        },
        uglify: {
            options: {
                mangle: true
            },
            my_target: {
                files: {
                    '../script.min.js': [
                        '../scripts/vendor/fontfaceobserver.js',
                        '../scripts/vendor/jquery.js',
                        '../scripts/vendor/fastclick.js',
                        '../scripts/vendor/ga.js',
                        '../scripts/vendor/twitter.js',
                        '../scripts/vendor/increment.js',
                        '../scripts/vendor/flowtype.js',
                        '../scripts/vendor/three.js',
                        // '../scripts/vendor/Projector.js',
                        // '../scripts/vendor/CanvasRenderer.js',
                        // '../scripts/vendor/ImprovedNoise.js',
                        '../scripts/land.js',
                        '../scripts/vendor/slab.js',
                        '../scripts/vendor/scrollreveal.js',
                        '../scripts/vendor/smooth-scrollbar.js',
                        '../scripts/script.js'
                    ]
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-bump');

    grunt.registerTask('default', ['uglify', 'cssmin', 'watch']);

    // Update On Save - Increase PreRelease Numbers
    var myNum = grunt.file.readJSON('package.json');
    var myNum = myNum.version;
    var lastNum = myNum.split('.')[2];
    if (lastNum > 25) {
        grunt.task.run('bump:minor');
    } else {
        grunt.task.run('bump:patch');
    }

}
