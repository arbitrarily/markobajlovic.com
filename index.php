﻿<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/includes.php');

$start = '2007-01-01';
// $winston_start = '2016-12-25';

$datetime1 = new DateTime($start);
// $datetime_winston = new DateTime($winston_start);
$datetime2 = new DateTime();

$interval = date_diff($datetime1, $datetime2);
// $winston_age = date_diff($datetime_winston, $datetime2);
$experience = $interval->format('%y');
// if ($winston_age->format('%y') > 0) {
//     $winston_age_formatted = $winston_age->format('%y year %m month');
// } else {
//     $winston_age_formatted = $winston_age->format('%m month');
// }

$tags = array(
    46 => 'fa-youtube-play',
    41 => 'fa-twitter',
    17 => 'fa-instagram',
    18 => 'fa-bookmark',
    54 => 'fa-tv',
    12 => 'fa-github',
    34 => 'fa-soundcloud',
    15 => 'fa-tumblr',
    39 => 'fa-tumblr'
);

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "http://mb4.in/wp-json/wp/v2/posts?per_page=15",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic YXBwOm1pbm9yLXhlYmVjLWRlbm91bmNlLXlhbW1lci1jb21wb3VuZC1sb3ktcmVja2xlc3Mtc3VldC1zcHVuLXF1aXRl",
    "cache-control: no-cache",
    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
  ),
));
$response = curl_exec($curl);
$response = json_decode($response);
curl_close($curl);

// Versioning
$json = 'grunt/package.json';
$version = json_decode( file_get_contents( $json ), true );
$version = ( $version['version'] ? $version['version'] : '0.9999' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
                                888                  888                d8b 888                   d8b
                               888                  888                Y8P 888                   Y8P
                               888                  888                    888
88888b.d88b.   8888b.  888d888 888  888  .d88b.     88888b.   8888b.  8888 888  .d88b.  888  888 888  .d8888b
888 "888 "88b     "88b 888P"   888 .88P d88""88b    888 "88b     "88b "888 888 d88""88b 888  888 888 d88P"
888  888  888 .d888888 888     888888K  888  888    888  888 .d888888  888 888 888  888 Y88  88P 888 888
888  888  888 888  888 888     888 "88b Y88..88P    888 d88P 888  888  888 888 Y88..88P  Y8bd8P  888 Y88b.
888  888  888 "Y888888 888     888  888  "Y88P"     88888P"  "Y888888  888 888  "Y88P"    Y88P   888  "Y8888P
                                                                       888
                                                                      d88P
                                                                    888P"
-->
<html xmlns="http://www.w3.org/1999/xhtml" data-version="<?php echo $version; ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="Description" content="Marko Bajlovic; multidisciplinary dev, designer, and creative amassing all classes of knowledge, expierence, and thought. Always learning, always creating.">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="author" content="Marko Bajlovic">
    <meta property="og:title" content="Marko Bajlovic" />
    <meta property="og:site_name" content="Marko Bajlovic" />
    <meta property="og:description" content="Marko Bajlovic; multidisciplinary dev, designer, and creative amassing all classes of knowledge, expierence, and thought. Always learning, always creating." />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://marko.tech" />
    <meta property="og:image" content="https://marko.tech/images/og.jpg" />
    <title>Marko Bajlovic</title>
    <link href="icon.png" rel="apple-touch-icon-precomposed" sizes="152x152">
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="css/font-awesome.css?ver=4.7.0" rel="stylesheet" type="text/css" media="screen" charset="utf-8">
    <link href="css/smooth-scrollbar.css?ver=7.4.1" rel="stylesheet" type="text/css" media="screen" charset="utf-8">
    <link href="style.min.css?ver=<?php echo $version; ?>" rel="stylesheet" type="text/css" media="screen" charset="utf-8">
</head>

<body>
    <div id="wrapper">
        <div id="container"></div>
        <div class="about-me" scrollbar>
            <div class="row">
                <div class="column align-middle small-12 medium-10 medium-centered">
                    <div class="name logo">
                        <h1>Marko&nbsp;Bajlovic</h1>
                    </div>
                    <div class="content">
                        <p>It's been <span class="the-sum" data-sum="<?php echo $experience; ?>"><span style="opacity:0;"><?php echo $experience; ?></span></span> years now that I've designing and developing graphics, code, and branding. I indulge myself in continued learning, figuring how things work, reading, design, cruising around on a skateboard, music discovery, mindfulness, and thinking creatively.</p>
                        <p class="min-height">Always building and designing, creating and learning; I continually strive to be <span class="roles">a <strong>jack of all trades</strong></span></p>
                        <div class="social content">
                            <nav class="social-nav">
                                <ul>
                                    <li>
                                        <a class="a" href="https://github.com/arbitrarily" target="_blank" title="Github">
                                            <i class="fa fa-github"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="a" href="https://bitbucket.org/arbitrarily/" target="_blank" title="Bitbucket">
                                            <i class="fa fa-bitbucket"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="a" href="http://twitter.com/desmosthenes" target="_blank" title="Twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="a" href="https://www.linkedin.com/in/markobajlovic/" target="_blank" title="Linkedin">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="a" href="http://t.markobajlovic.com" target="_blank" title="My Tumblr - What inspires Me">
                                            <i class="fa fa-tumblr"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="a" href="https://instagram.com/markobajlovic" target="_blank" title="Images on Instagram">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="a" href="http://last.fm/user/desmosthenes" target="_blank" title="What music do I listen to?">
                                            <i class="fa fa-lastfm"></i>
                                        </a>
                                    </li>
                                    <li class="trakt">
                                        <a class="a" href="https://trakt.tv/users/arbitrarily" target="_blank" title="What I watch">
                                            <img src="/images/logos/trakt.png">
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="name subtitle a">
                        <p>Highlight Projects</p>
                    </div>
                    <div class="content">
                        <nav class="nav">
                            <ul>
                                <li class="a">
                                    <a href="http://odysia.co" target="_blank">
                                        <h3>ODYSIA</h3>
                                        <p>ODYSIA is a creative agency specializing in emerging tech, digital platforms, marketing, and story-telling.</p>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://etherinthenews.com" target="_blank">
                                        <h3>Ether in the News</h3>
                                        <p>All ethereum news, videos, and current stats and numbers; all the time.</p>
                                    </a>
                                </li>
                            </ul>
                            <ul>
                                <li class="a">
                                    <a href="http://lexichronic.com" target="_blank">
                                        <h3>Lexichronic</h3>
                                        <p>#lexichronic is back. Latest, curated marijuana news, culture, videos, stats, and facts; updated constantly.</p>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://mb4.in" target="_blank">
                                        <h3>mb4.in</h3>
                                        <p>mb4.in is a site which collects all of my social postings and interactions into one endless feed.</p>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="name subtitle a">
                        <p>Personal Projects</p>
                    </div>
                    <div class="content works">
                        <nav>
                            <ul>
                                <li class="a">
                                    <span>Veza</span>
                                </li>
                                <li class="a">
                                    <span>Shepherd</span>
                                </li>
                                <li class="a">
                                    <span>Coinbase Auto-trader</span>
                                </li>
                                <li class="a">
                                    <span>Twitter Audience Builder</span>
                                </li>
                                <li class="a">
                                    <a href="http://t.markobajlovic.com" target="_blank">
                                        <span>Inspiration Tumblr</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="https://roserenegades.com" target="_blank">
                                        <span>Rose Renegades</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://adventuresofwinston.com" target="_blank">
                                        <span>Adventures of Winston</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://gallery.marko.tech" target="_blank">
                                        <span>Panoramic Gallery</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://aggro.news" target="_blank">
                                        <span>Aggro News</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <span>O&Uh</span>
                                </li>
                                <li class="a">
                                    <a href="http://tvti.me" target="_blank">
                                        <span>tvti.me</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://yvngdrvgn.com" target="_blank">
                                        <span>yvngdrvgn</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://manuallfocus.com" target="_blank">
                                        <span>Manuall Focus</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://nodinosaurs.com" target="_blank">
                                        <span>No Dinosaurs</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://anikacare.com/" target="_blank">
                                        <span>Anika Care</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <span>Science Greats</span>
                                </li>
                                <li class="a">
                                    <span>Summer StagePass</span>
                                </li>
                                <li class="a">
                                    <span>Cedar Lane</span>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="name subtitle a">
                        <p>Previous Clients/Projects</p>
                    </div>
                    <div class="content works">
                        <nav>
                            <ul>
                                <li class="a">
                                    <a href="http://rpvac.marko.tech" target="_blank">
                                        <span>RPEMS</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://etherealsummit.com" target="_blank">
                                        <span>Ethereal Summit SF</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://darkhorsestudios.nyc" target="_blank">
                                        <span>Dark Horse Studios</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="https://thefoundersorganization.com" target="_blank">
                                        <span>The Founder's Organization</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://entethalliance.org" target="_blank">
                                        <span>Enterprise Ethereum Alliance</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://oceanic.global" target="_blank">
                                        <span>Oceanic Global</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://etherealsummit.com" target="_blank">
                                        <span>Ethereal Summit BK</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://dayoneventures.co" target="_blank">
                                        <span>Day One Ventures</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://soraspy.com" target="_blank">
                                        <span>So Raspy</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://badderink.com" target="_blank">
                                        <span>Badder Ink</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://blackocean.com" target="_blank">
                                        <span>Black Ocean</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://reelio.com" target="_blank">
                                        <span>Reelio</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://www.nextravel.com" target="_blank">
                                        <span>Nextravel</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://theartkartelnyc.com" target="_blank">
                                        <span>Art Kartel</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://elitedaily.com/" target="_blank">
                                        <span>Elite Daily</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://zacklevandov.com/" target="_blank">
                                        <span>Zack Levandov</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <span>The Edge</span>
                                </li>
                                <li class="a">
                                    <span>TXP</span>
                                </li>
                                <li class="a">
                                    <span>By Any Means</span>
                                </li>
                                <li class="a">
                                    <span>BMG</span>
                                </li>
                                <li class="a">
                                    <span>Adore Me</span>
                                </li>
                                <li class="a">
                                    <span>American Express</span>
                                </li>
                                <li class="a">
                                    <span>Rich World Ent</span>
                                </li>
                                <li class="a">
                                    <span>InHub</span>
                                </li>
                                <li class="a">
                                    <span>Samsung</span>
                                </li>
                                <li class="a">
                                    <span>DKab</span>
                                </li>
                                <li class="a">
                                    <span>The Sickest</span>
                                </li>
                                <li class="a">
                                    <span>Sayo Takegami</span>
                                </li>
                                <li class="a">
                                    <span>The Dishh</span>
                                </li>
                                <li class="a">
                                    <a href="http://whatsaroundtheworld.com" target="_blank"></a>
                                        <span>What's Around the World?</span>
                                    </li>
                                </li>
                                <li class="a">
                                    <span>iSnapRentals</span>
                                </li>
                                <li class="a">
                                    <span>Hood Chef</span>
                                </li>
                                <li class="a">
                                    <span>New NYer</span>
                                </li>
                                <li class="a">
                                    <span>Patricia Whitter</span>
                                </li>
                                <li class="a">
                                    <span>Tpstr</span>
                                </li>
                                <li class="a">
                                    <span>Mom Bible</span>
                                </li>
                                <li class="a">
                                    <span>Kurately</span>
                                </li>
                                <li class="a">
                                    <span>Honesty for Breakfast</span>
                                </li>
                                <li class="a">
                                    <span>Art Meets Life</span>
                                </li>
                                <li class="a">
                                    <span>XXV/0</span>
                                </li>
                                <li class="a">
                                    <span>The Presh Life</span>
                                </li>
                                <li class="a">
                                    <span>Henry Knows</span>
                                </li>
                                <li class="a">
                                    <span>The Slow Way</span>
                                </li>
                                <li class="a">
                                    <span>Quote Fountain</span>
                                </li>
                                <li class="a">
                                    <span>It's a Tots Life</span>
                                </li>
                                <li class="a">
                                    <span>The Daily Heel</span>
                                </li>
                                <li class="a">
                                    <span>Savour the Taste</span>
                                </li>
                                <li class="a">
                                    <span>Lady Lookout</span>
                                </li>
                                <li class="a">
                                    <span>The Finer List</span>
                                </li>
                                <li class="a">
                                    <span>Finishoes</span>
                                </li>
                                <li class="a">
                                    <span>Faktsy</span>
                                </li>
                                <li class="a">
                                    <span>Vidigamers</span>
                                </li>
                                <li class="a">
                                    <span>New York Red Bulls</span>
                                </li>
                                <li class="a">
                                    <span>Powered by Mom</span>
                                </li>
                                <li class="a">
                                    <span>Sweet Pea Savings</span>
                                </li>
                                <li class="a">
                                    <span>We Here Yet?</span>
                                </li>
                                <li class="a">
                                    <span>TomorrowWorld</span>
                                </li>
                                <li class="a">
                                    <span>Group Modelo</span>
                                </li>
                                <li class="a">
                                    <span>trending.is</span>
                                </li>
                                <li class="a">
                                    <span>Farm to Fork Meals</span>
                                </li>
                                <li class="a">
                                    <span>Wisemarkit</span>
                                </li>
                                <li class="a">
                                    <span>Super Mario Run Tips</span>
                                </li>
                                <li class="a">
                                    <a href="http://ninaberlingeri.com/" target="_blank">
                                        <span>Nina Berlingeri</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <a href="http://awkwardfamilyphotos.com/" target="_blank">
                                        <span>Awkward Family Photos</span>
                                    </a>
                                </li>
                                <li class="a">
                                    <span>The Lifestyle Press</span>
                                </li>
                                <li class="a">
                                    <span>Grooveshark</span>
                                </li>
                                <li class="a">
                                    <span>Essex Computers</span>
                                </li>
                                <li class="a">
                                    <span>Parsons The New School</span>
                                </li>
                                <li class="a">
                                    <span>NAPMG</span>
                                </li>
                                <li class="a">
                                    <span>Arbitrarily iPhone Themes on Cydia</span>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <?php if ( $response ): ?>
                    <div class="name subtitle a small-12">
                        <p>Latest Social</p>
                    </div>
                    <div class="content social-content">
                        <?php
                        $count = 0;
                        foreach ($response as $r) {
                            if ( $count > 15 ) break;
                            $link = $r->link;
                            $link = filter_var($link, FILTER_SANITIZE_URL);
                            $title = ( $r->title->rendered ? $r->title->rendered : false );
                            $tag = ( $tags[$r->tags[0]] ? $tags[$r->tags[0]] : false );
                            if ( $link && $title ) {
                                echo '<a href="' . $link . '" target="_blank">';
                                if ($tag) {
                                    echo '<i class="fa ' . trim( $tag ) . '"></i>';
                                } else {
                                    echo '<i class="fa fa-link"></i>';
                                }
                                echo $title;
                                echo '</a>';
                            }
                            $count++;
                        }
                        ?>
                    </div>
                    <?php endif; ?>
                    <div class="name subtitle a small-12">
                        <p>Contact</p>
                    </div>
                    <nav class="contact-nav">
                        <ul class="content contact columns">
                            <li class="a">
                                <a href="mailto:mb@markobajlovic.com" title="Shoot me an e-mail">
                                    <span>E-Mail</span>
                                </a>
                            </li>
                            <li class="a">
                                <a href="https://twitter.com/intent/tweet?screen_name=desmosthenes&text=Hey" target="_blank" title="Tweet @desmosthenes">
                                    <span>Tweet</span>
                                </a>
                            </li>
                            <li class="d a">
                                <a href="http://www.diabloprogress.com/hero/des-1278/Des/46703173" target="_blank" title="My Wizard">
                                    <span>Diablo III</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script type="text/javascript" src="script.min.js?ver=<?php echo $version; ?>"></script>

</body>

</html>
